#!/usr/bin/perl

use strict;
use warnings "all";

my $str = `ceph osd pool stats`;
my @arr = split(/\n\n/, $str);

my $msg = "{\n    \"data\": [\n";

foreach my $p (@arr) {
        my  @str = split(/\s+/, $p);
        next if ((not defined($str[1])) or ($str[1] eq ''));
        next if ((not defined($str[3])) or ($str[3] eq ''));
{# in jinja template engine this is comment, so in real script fix this #}
	$msg .= sprintf("        {\n            \"\{#POOLID}\": \"%s\",\n", $str[3]);
        $msg .= sprintf("            \"\{#POOLNAME}\": \"%s\"\n        },\n", $str[1]);
}
chop($msg);
chop($msg);

$msg .= "\n    ]\n}\n";
print $msg;

