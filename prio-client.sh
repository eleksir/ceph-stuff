#!/bin/bash

OSD="$1"

if [ "$OSD" = "" ]; then
        echo "OSD ID is not set"
        exit 0
fi

echo "==> $OSD"
ceph tell osd.$OSD injectargs '--osd_recovery_op_priority 1'
ceph tell osd.$OSD injectargs '--osd_recovery_threads 6'
ceph tell osd.$OSD injectargs '--osd_recovery_max_active 1'
ceph tell osd.$OSD injectargs '--osd_max_backfills 1'

