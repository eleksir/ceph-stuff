#!/usr/bin/perl

use warnings "all";
use strict;
use Fcntl;

sub get_ceph_status(); # defined - not healthy | undef - healthy
sub mark_osd_out($);
sub logger($);
sub restore_prio(@);

if ((int(@ARGV) < 1) or ($ARGV[0] eq '--help') or ($ARGV[0] eq '-h')) {
	print "\n    Usage:   $0 osd_num osd_num osd_num\n\n";
	print "    Example: user\@host\# $0 10 25 44 128 9 36 33 78 91\n\n";
	exit 0;
}

my @osd_list = @ARGV;
my $PACK = 2;
my $OSDINDEX = 0;

while ( 1 ) {
	while ($PACK >0) {
		if (defined($osd_list[$OSDINDEX])) {
			logger("Mark $osd_list[$OSDINDEX] as in");
			mark_osd_in($osd_list[$OSDINDEX]);
		} else {
# last (and not full) pack of osds, we _can_ quit now
			logger("All OSD marked as in");
			exit 0;
		}

		$OSDINDEX = $OSDINDEX + 1;
		$PACK = $PACK - 1;
		sleep 300;
	}

	while (defined(get_ceph_status())) {
		sleep 60;
	}

	$PACK = 2;
	restore_prio(@osd_list);
}

sub get_ceph_status() {
	my $status = `/bin/ceph -s`;
	my @msg = split(/\n/, $status);
# by default we assume that ceph is unhealthy, with misplaced pgs, and degraded pgs
	my $healthy = undef;
	my $misplaced = undef;
	my $degraded = undef;

	foreach (@msg) {
		return undef if ($_ =~ /health HEALTH_OK$/);
		$degraded = 1 if ($_ =~ /recovery \d+\/\d+ objects degraded/);
		$misplaced = 1 if ($_ =~ /recovery \d+\/\d+ objects misplaced/)
	}

	if ((defined($degraded)) or (defined($misplaced))) {
		return 1;
	}
	
	return undef;
}
	
sub mark_osd_in($) {
	my $osd = shift;
	`/bin/ceph osd in $osd`;
	sleep(90);
	`./prio-recovery.sh $osd`;
}
	
sub logger($) {
	my $msg = shift;
	syswrite STDOUT, "$msg\n";
}

sub restore_prio(@) {
	my @osds = @_;

	foreach(@osds) {
		`./prio-client.sh $_`
	}
}

