#!/usr/bin/perl
#
use strict;
use warnings "all";
use JSON::PP;

my $jstr = `ceph osd tree --format=json`;

my $json = decode_json $jstr;

my @nodes = @{ $json->{'nodes'} };

foreach my $node (@nodes) {
    if ($node->{'type'} eq 'host') {
	    print "Хост: $node->{'name'}\n";
	    my @children = @{ $node->{'children'} };
	
	    foreach my $child (@children) {
		    print "Диск: osd.$child\n";
	    }
    }
}
