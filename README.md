## CEPH storage helper scripts

* ceph-osd-list.pl - another listing presetation of listing node<->osd
* ceph_osd_kickin.pl - kicks in big amount of osds in cluster by small batches
* ceph_osd_kickoff_mt.pl - ejects big amount of osds from cluster by small batches
* ceph_osd_pool_discovery.pl - script for zabbix' pool discovery
* ceph_osd_pool_stats.pl - zabbix monitoring script
* ceph_reweight.pl - carefuly reweights osd when "near full osd" cluster health warning appears
* prio-client.sh - gives priority to client io operations
* prio-recovery.sh - gives priority to recovery io operations
