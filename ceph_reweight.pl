#!/usr/bin/perl
#
# This script reweights osd by 0.02 if utilization overgrows 85%
# it also reweight osd 80% full osds by 0.01 as a "bonus".
# It assumes that ceph cluster is less than 75-77% packed with data. 

use strict;
use warnings "all";
use Fcntl;

use JSON::PP;
use Data::Dumper;
sub help();

my $dry = 1;

if (defined($ARGV[0]) and ($ARGV[0] eq '--do-it')) {
	my $jstr = `ceph osd df --format=json-pretty`;
	$jstr =~ s/\-nan/0/gi; # ceph tends to return broken json if custer is not in perfect shape
	my $j = decode_json $jstr;

	foreach my $osd (@{$j->{'nodes'}}) {
# skip all unrelated objects
		next unless (defined($osd->{'utilization'}));
# skip all objects that are not 80% full
		next if ($osd->{'utilization'} < 80.000001);
		my $new_weight = 1;

		if (($osd->{'utilization'} >= 80.000001) and ($osd->{'utilization'} < 85.000001)) {
			$new_weight = $osd->{'reweight'} - 0.01;
			printf ("osd.%s is %02.6f%% full, reweight it %01.6f -> %01.6f\n", $osd->{'id'}, $osd->{'utilization'}, $osd->{'reweight'}, $new_weight);
		}

		if ($osd->{'utilization'} >= 85.000001) {
			$new_weight = $osd->{'reweight'} - 0.02;
			printf ("osd.%s is %02.6f%% full, reweight it %01.6f -> %01.6f\n", $osd->{'id'}, $osd->{'utilization'}, $osd->{'reweight'}, $new_weight);
		}

		if (defined($ARGV[1])) {
			help () if ($ARGV[1] ne '--dry');
		} else {
			system('ceph', 'osd', 'reweght', sprintf("%d", $osd->{'id'}), sprintf("%f", $new_weight));
		}
	}
} else {
	help();
}

sub help() {
	print <<MSG;
Usage: 
$0			prints this help
$0 --do-it		just reweghts osds
$0 --do-it --dry	will print what should be done

it reweghts 80% full osds by 0.01 and 85%+ full osds by 0.02

Note: you should wait until rebalance ends in order to understand state of
cluster utilization.
MSG

	return undef;
}

exit 0;
