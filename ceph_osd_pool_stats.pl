#!/usr/bin/perl

use strict;
use warnings "all";

sub usage();
# print usage banner if we have not enough arg
usage() if ((not defined($ARGV[1])) or ($ARGV[1] eq ''));

my $pflag = 0;
my $paramflag = 0;

my $str = `ceph osd pool stats`;
my @arr = split(/\n\n/, $str);

my $msg = "{\n    \"data\": [\n";

foreach my $p (@arr) {
	next unless (defined((split(/\s+/, $p))[1]));
	next unless ((split(/\s+/, $p))[1] eq $ARGV[0]);

	if ((split(/\s+/, $p))[4] eq 'nothing') {
		syswrite STDOUT, '0';
		exit 0;
	}

	my @data = split(/\n/, $p);

	foreach my $l (@data) {
		if (substr($l, 2, 6) eq 'client') {
			if ($ARGV[1] eq 'crops') {
				syswrite STDOUT, (split(/\s/, $l))[10];
			} elsif ($ARGV[1] eq 'cwops') {
				syswrite STDOUT, (split(/\s/, $l))[13];
			} elsif ($ARGV[1] eq 'crbps') {
				if ((split(/\s/, $l))[5] =~ /^k/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024;
				} elsif ((split(/\s/, $l))[5] =~ /^M/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024 * 1024;
				} elsif ((split(/\s/, $l))[5] =~ /^G/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024 * 1024 * 1024;
				}
			} elsif ($ARGV[1] eq 'cwbps') {
				if ((split(/\s/, $l))[5] =~ /^k/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024;
				} elsif ((split(/\s/, $l))[5] =~ /^M/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024 * 1024;
				} elsif ((split(/\s/, $l))[5] =~ /^G/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024 * 1024 * 1024;
				}
			}
		} elsif (substr($l, 2, 8) eq 'recovery') {
			if ($ARGV[1] eq 'rbps') {
				if ((split(/\s/, $l))[5] =~ /^k/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024;
				} elsif ((split(/\s/, $l))[5] =~ /^M/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024 * 1024;
				} elsif ((split(/\s/, $l))[5] =~ /^G/) {
					syswrite STDOUT, (split(/\s/, $l))[4] * 1024 * 1024 * 1024;
				} else {
					# i never seen stats in bytes per second, so as an exception printout just zero
					syswrite STDOUT, '0';
				}
			}
		}
	}
}

exit 0;

sub usage () {
	print <<EOF;
Usage: $0 <poolname> <parameter>

Where parameter can be:
crops - client reads per second
cwops - client writes per second
crbps - client read kbytes per second
cwbps - client write kbytes per second
rbps - recovery io in bytes per second
EOF
	exit 0;
}

__END__

pool vms.sata.zone3 id 13
  nothing is going on

pool volumes.sata.zone1 id 14
  recovery io 1418 MB/s, 360 objects/s
  client io 24445 kB/s rd, 37316 kB/s wr, 975 op/s rd, 1115 op/s wr

