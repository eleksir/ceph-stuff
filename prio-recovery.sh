#!/bin/bash

OSD="$1"

if [ "$OSD" = "" ]; then
        echo "OSD ID is not set"
        exit 0
fi

ceph tell osd.$OSD injectargs '--osd_recovery_op_priority 10'
ceph tell osd.$OSD injectargs '--osd_recovery_threads 10'
ceph tell osd.$OSD injectargs '--osd_recovery_max_active 3'
ceph tell osd.$OSD injectargs '--osd_max_backfills 10'

